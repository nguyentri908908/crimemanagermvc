﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CrimeManager.Models;
using CrimeManager.Helper;

namespace CrimeManager.Controllers
{
    public class ThamNuoiController : Controller
    {
        private readonly DbCrimeManageContext _context;

        public ThamNuoiController(DbCrimeManageContext context)
        {
            _context = context;
        }

        // GET: ThamNuoi
        public async Task<IActionResult> Index()
        {
            var session = HttpContext.Session.GetObject<UserSession>("OpenSession");
            if (session.UserDetail != null)
            {
                if (session.UserDetail.IdUser == 4)
                {
                    ViewBag.DiaDiem = await _context.TbDiaDiemThamNuois.ToListAsync();
                    ViewBag.ToiPham = await _context.TbTuNhans.ToListAsync();
                    var dbCrimeManageContext = _context.TbThamNuois.Include(t => t.IdDiaDiemNavigation).Include(t => t.IdToiPhamNavigation);
                    return View(await dbCrimeManageContext.ToListAsync());
                }
                else
                {
                    List<int> diaDiem = _context.TbCanBoDiaDiems.Where(item => item.IdCanBo
                        == session.UserDetail.IdCanBo && item.ThoiGianBd.Value.Date == DateTime.Today).
                        Select(x => x.IdDiaDiem.Value).ToList();
           
                    var dbCrimeManageContext = _context.TbThamNuois.Where(item => diaDiem.Contains(item.IdDiaDiem.Value)
                        && item.ThoiGianBd.Value.Date == DateTime.Now);
                    return View(await dbCrimeManageContext.ToListAsync());
                }
            }
            return View();
           
        }

        // GET: ThamNuoi/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.TbThamNuois == null)
            {
                return NotFound();
            }

            var tbThamNuoi = await _context.TbThamNuois
                .Include(t => t.IdDiaDiemNavigation)
                .Include(t => t.IdToiPhamNavigation)
                .FirstOrDefaultAsync(m => m.IdThamNuoi == id);
            if (tbThamNuoi == null)
            {
                return NotFound();
            }
            ViewData["IdDiaDiem"] = new SelectList(_context.TbDiaDiemThamNuois, "IdDiaDiem", "DiaDiem", tbThamNuoi.IdDiaDiem);
            ViewData["IdToiPham"] = new SelectList(_context.TbTuNhans, "IdTuNhan", "MaTuNhan", tbThamNuoi.IdToiPham);
            return View(tbThamNuoi);
        }

        // GET: ThamNuoi/Create
        public IActionResult Create()
        {
            ViewData["IdDiaDiem"] = new SelectList(_context.TbDiaDiemThamNuois, "IdDiaDiem", "DiaDiem");
            ViewData["IdToiPham"] = new SelectList(_context.TbTuNhans, "IdTuNhan", "MaTuNhan");
            return View();
        }

        // POST: ThamNuoi/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdThamNuoi,IdToiPham,TenNguoiThamNuoi,CccdnguoiTham,IdDiaDiem,ThoiGianBd,ThoiGianKt")] TbThamNuoi tbThamNuoi)
        {

                _context.Add(tbThamNuoi);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            
        }

        // GET: ThamNuoi/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.TbThamNuois == null)
            {
                return NotFound();
            }

            var tbThamNuoi = await _context.TbThamNuois.FindAsync(id);
            if (tbThamNuoi == null)
            {
                return NotFound();
            }
            ViewData["IdDiaDiem"] = new SelectList(_context.TbDiaDiemThamNuois, "IdDiaDiem", "DiaDiem", tbThamNuoi.IdDiaDiem);
            ViewData["IdToiPham"] = new SelectList(_context.TbTuNhans, "IdTuNhan", "MaTuNhan", tbThamNuoi.IdToiPham);
            return View(tbThamNuoi);
        }

        // POST: ThamNuoi/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdThamNuoi,IdToiPham,TenNguoiThamNuoi,CccdnguoiTham,IdDiaDiem,ThoiGianBd,ThoiGianKt")] TbThamNuoi tbThamNuoi)
        {
            if (id != tbThamNuoi.IdThamNuoi)
            {
                return NotFound();
            }

           
                try
                {
                    _context.Update(tbThamNuoi);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TbThamNuoiExists(tbThamNuoi.IdThamNuoi))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            
            
        }

        // GET: ThamNuoi/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.TbThamNuois == null)
            {
                return NotFound();
            }

            var tbThamNuoi = await _context.TbThamNuois
                .Include(t => t.IdDiaDiemNavigation)
                .Include(t => t.IdToiPhamNavigation)
                .FirstOrDefaultAsync(m => m.IdThamNuoi == id);
            if (tbThamNuoi == null)
            {
                return NotFound();
            }
            ViewData["IdDiaDiem"] = new SelectList(_context.TbDiaDiemThamNuois, "IdDiaDiem", "DiaDiem", tbThamNuoi.IdDiaDiem);
            ViewData["IdToiPham"] = new SelectList(_context.TbTuNhans, "IdTuNhan", "MaTuNhan", tbThamNuoi.IdToiPham);
            return View(tbThamNuoi);
        }

        // POST: ThamNuoi/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.TbThamNuois == null)
            {
                return Problem("Entity set 'DbCrimeManageContext.TbThamNuois'  is null.");
            }
            var tbThamNuoi = await _context.TbThamNuois.FindAsync(id);
            if (tbThamNuoi != null)
            {
                _context.TbThamNuois.Remove(tbThamNuoi);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TbThamNuoiExists(int id)
        {
          return (_context.TbThamNuois?.Any(e => e.IdThamNuoi == id)).GetValueOrDefault();
        }
    }
}
