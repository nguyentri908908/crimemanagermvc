﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CrimeManager.Models;
using CrimeManager.Helper;

namespace CrimeManager.Controllers
{
    public class CanBoPhongGiamController : Controller
    {
        private readonly DbCrimeManageContext _context;

        public CanBoPhongGiamController(DbCrimeManageContext context)
        {
            _context = context;
        }

        // GET: CanBoPhongGiam
        public async Task<IActionResult> Index()
        {
            var session = HttpContext.Session.GetObject<UserSession>("OpenSession");
            if (session.UserDetail != null)
            {
                if (session.UserDetail.IdUser == 4)
                {
                    ViewBag.CanBo = await _context.TbCanBos.ToListAsync();
                    ViewBag.PhongGiam = await _context.TbPhongGiams.ToListAsync();
                    var dbCrimeManageContext = _context.TbCanBoPhongGiams.Include(t => t.IdCanBoNavigation).Include(t => t.IdPhongGiamNavigation);
                    return View(await dbCrimeManageContext.ToListAsync());
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            return View();
            
        }

        // GET: CanBoPhongGiam/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.TbCanBoPhongGiams == null)
            {
                return NotFound();
            }

            var tbCanBoPhongGiam = await _context.TbCanBoPhongGiams
                .Include(t => t.IdCanBoNavigation)
                .Include(t => t.IdPhongGiamNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tbCanBoPhongGiam == null)
            {
                return NotFound();
            }
            ViewData["IdCanBo"] = new SelectList(_context.TbCanBos, "IdCanBo", "TenCanBo", tbCanBoPhongGiam.IdCanBo);
            ViewData["IdPhongGiam"] = new SelectList(_context.TbPhongGiams, "IdPhongGiam", "TenPhong", tbCanBoPhongGiam.IdPhongGiam);
            return View(tbCanBoPhongGiam);
        }

        // GET: CanBoPhongGiam/Create
        public IActionResult Create()
        {
            ViewData["IdCanBo"] = new SelectList(_context.TbCanBos, "IdCanBo", "TenCanBo");
            ViewData["IdPhongGiam"] = new SelectList(_context.TbPhongGiams, "IdPhongGiam", "TenPhong");
            return View();
        }

        // POST: CanBoPhongGiam/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,IdCanBo,IdPhongGiam,ThoiGianBd,ThoiGianKt")] TbCanBoPhongGiam tbCanBoPhongGiam)
        {
          TbCanBoPhongGiam check = await _context.TbCanBoPhongGiams.Where(t => t.IdCanBo==tbCanBoPhongGiam.IdCanBo&&t.IdPhongGiam
          ==tbCanBoPhongGiam.IdPhongGiam&&t.ThoiGianBd.Value==tbCanBoPhongGiam.ThoiGianBd.Value && t.ThoiGianKt.Value == tbCanBoPhongGiam.ThoiGianKt.Value).FirstOrDefaultAsync();
            if(check == null) {
                _context.Add(tbCanBoPhongGiam);
                await _context.SaveChangesAsync();
            }
                return RedirectToAction(nameof(Index));
           
        }

        // GET: CanBoPhongGiam/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.TbCanBoPhongGiams == null)
            {
                return NotFound();
            }

            var tbCanBoPhongGiam = await _context.TbCanBoPhongGiams.FindAsync(id);
            if (tbCanBoPhongGiam == null)
            {
                return NotFound();
            }
            ViewData["IdCanBo"] = new SelectList(_context.TbCanBos, "IdCanBo", "TenCanBo", tbCanBoPhongGiam.IdCanBo);
            ViewData["IdPhongGiam"] = new SelectList(_context.TbPhongGiams, "IdPhongGiam", "TenPhong", tbCanBoPhongGiam.IdPhongGiam);
            return View(tbCanBoPhongGiam);
        }

        // POST: CanBoPhongGiam/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,IdCanBo,IdPhongGiam,ThoiGianBd,ThoiGianKt")] TbCanBoPhongGiam tbCanBoPhongGiam)
        {
            if (id != tbCanBoPhongGiam.Id)
            {
                return NotFound();
            }
                try
                {
                TbCanBoPhongGiam check = await _context.TbCanBoPhongGiams.Where(t => t.IdCanBo == tbCanBoPhongGiam.IdCanBo && t.IdPhongGiam
         == tbCanBoPhongGiam.IdPhongGiam && t.ThoiGianBd.Value == tbCanBoPhongGiam.ThoiGianBd.Value && t.ThoiGianKt.Value == tbCanBoPhongGiam.ThoiGianKt.Value).FirstOrDefaultAsync();
                if (check == null)
                {
                    _context.Update(tbCanBoPhongGiam);
                    await _context.SaveChangesAsync();
                }
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TbCanBoPhongGiamExists(tbCanBoPhongGiam.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
        }

        // GET: CanBoPhongGiam/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.TbCanBoPhongGiams == null)
            {
                return NotFound();
            }

            var tbCanBoPhongGiam = await _context.TbCanBoPhongGiams
                .Include(t => t.IdCanBoNavigation)
                .Include(t => t.IdPhongGiamNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tbCanBoPhongGiam == null)
            {
                return NotFound();
            }
            ViewData["IdCanBo"] = new SelectList(_context.TbCanBos, "IdCanBo", "TenCanBo", tbCanBoPhongGiam.IdCanBo);
            ViewData["IdPhongGiam"] = new SelectList(_context.TbPhongGiams, "IdPhongGiam", "TenPhong", tbCanBoPhongGiam.IdPhongGiam);
            return View(tbCanBoPhongGiam);
        }

        // POST: CanBoPhongGiam/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.TbCanBoPhongGiams == null)
            {
                return Problem("Entity set 'DbCrimeManageContext.TbCanBoPhongGiams'  is null.");
            }
            var tbCanBoPhongGiam = await _context.TbCanBoPhongGiams.FindAsync(id);
            if (tbCanBoPhongGiam != null)
            {
                _context.TbCanBoPhongGiams.Remove(tbCanBoPhongGiam);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TbCanBoPhongGiamExists(int id)
        {
          return (_context.TbCanBoPhongGiams?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
