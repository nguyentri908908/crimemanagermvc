﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CrimeManager.Models;

namespace CrimeManager.Controllers
{
    public class ThongTinTuNhanController : Controller
    {
        private readonly DbCrimeManageContext _context;

        public ThongTinTuNhanController(DbCrimeManageContext context)
        {
            _context = context;
        }

        // GET: ThongTinTuNhan
        public async Task<IActionResult> Index()
        {
              return _context.TbThongTinTuNhans != null ? 
                          View(await _context.TbThongTinTuNhans.ToListAsync()) :
                          Problem("Entity set 'DbCrimeManageContext.TbThongTinTuNhans'  is null.");
        }

        // GET: ThongTinTuNhan/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.TbThongTinTuNhans == null)
            {
                return NotFound();
            }

            var tbThongTinTuNhan = await _context.TbThongTinTuNhans
                .FirstOrDefaultAsync(m => m.IdThongTin == id);
            if (tbThongTinTuNhan == null)
            {
                return NotFound();
            }
            List<TbBanAn> lstBanAn = _context.TbBanAns.Where(item=>item.IdPhamNhan==id).ToList();
            ViewBag.lstBanAn = lstBanAn;
            ViewBag.Loai = _context.TbLoaiBanAns.ToList();
            return View(tbThongTinTuNhan);
        }

        // GET: ThongTinTuNhan/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: ThongTinTuNhan/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdThongTin,HoTen,NgaySinh,QueQuan,Cccd")] TbThongTinTuNhan tbThongTinTuNhan)
        {
            if (ModelState.IsValid)
            {
                _context.Add(tbThongTinTuNhan);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(tbThongTinTuNhan);
        }

        // GET: ThongTinTuNhan/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.TbThongTinTuNhans == null)
            {
                return NotFound();
            }

            var tbThongTinTuNhan = await _context.TbThongTinTuNhans.FindAsync(id);
            if (tbThongTinTuNhan == null)
            {
                return NotFound();
            }
            return View(tbThongTinTuNhan);
        }

        // POST: ThongTinTuNhan/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdThongTin,HoTen,NgaySinh,QueQuan,Cccd")] TbThongTinTuNhan tbThongTinTuNhan)
        {
            if (id != tbThongTinTuNhan.IdThongTin)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(tbThongTinTuNhan);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TbThongTinTuNhanExists(tbThongTinTuNhan.IdThongTin))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(tbThongTinTuNhan);
        }

        // GET: ThongTinTuNhan/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.TbThongTinTuNhans == null)
            {
                return NotFound();
            }

            var tbThongTinTuNhan = await _context.TbThongTinTuNhans
                .FirstOrDefaultAsync(m => m.IdThongTin == id);
            if (tbThongTinTuNhan == null)
            {
                return NotFound();
            }

            return View(tbThongTinTuNhan);
        }

        // POST: ThongTinTuNhan/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.TbThongTinTuNhans == null)
            {
                return Problem("Entity set 'DbCrimeManageContext.TbThongTinTuNhans'  is null.");
            }
            var tbThongTinTuNhan = await _context.TbThongTinTuNhans.FindAsync(id);
            if (tbThongTinTuNhan != null)
            {
                _context.TbThongTinTuNhans.Remove(tbThongTinTuNhan);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TbThongTinTuNhanExists(int id)
        {
          return (_context.TbThongTinTuNhans?.Any(e => e.IdThongTin == id)).GetValueOrDefault();
        }
    }
}
