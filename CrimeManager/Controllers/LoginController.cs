﻿using CrimeManager.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Text;
using System.Security.Cryptography;
using CrimeManager.Helper;
namespace CrimeManager.Controllers
{
    public class LoginController : Controller
    {
        private readonly DbCrimeManageContext _context;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public LoginController(DbCrimeManageContext context, IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            _httpContextAccessor = httpContextAccessor;
        }
        public IActionResult Index()
        {
            ViewBag.IsLoginPage = true; // Đây là trang đăng nhập
            return View();
        }
        static string ComputeSHA256Hash(string input)
        {
            using (SHA256 sha256 = SHA256.Create())
            {
                // Chuyển đổi chuỗi thành mảng byte
                byte[] inputBytes = Encoding.UTF8.GetBytes(input);

                // Tính toán giá trị băm cho mảng byte
                byte[] hashBytes = sha256.ComputeHash(inputBytes);

                // Chuyển đổi mảng byte thành chuỗi hex
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    builder.Append(hashBytes[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }
            
        public async Task<IActionResult> Success()
        {
            // Lấy giá trị của tên người dùng từ form
            string username = HttpContext.Request.Form["username"];
            // Lấy giá trị của mật khẩu từ form
            string password = HttpContext.Request.Form["password"];
            TbUser user = _context.TbUsers.Where(item => (item.UserName.Equals(username) && item.Password.Equals(ComputeSHA256Hash(password)))
            ).FirstOrDefault();
            if (user!=null)
            {
                UserSession session = new UserSession(user);
                //create a session object named whatever you want and pass the usersession as a value
                _httpContextAccessor.HttpContext.Session.SetObject("OpenSession", session);
                // Nếu đăng nhập thành công, chuyển hướng đến trang chính
                return RedirectToAction("Index", "Home");
            }
            else
            {
                // Xử lý khi đăng nhập không thành công
                return RedirectToAction("Index", "Login");
            }

        }
        public async Task<IActionResult> Register()
        {
            string username = HttpContext.Request.Form["username"];
            // Lấy giá trị của mật khẩu từ form
            string password = HttpContext.Request.Form["password"];
            TbUser newUser = new TbUser();
            newUser.UserName = username;
            newUser.Password = ComputeSHA256Hash(password);
            TbUser checkHasUser = _context.TbUsers.Where(item=>item.UserName.Equals(newUser.UserName))
                .FirstOrDefault();
            if (checkHasUser == null)
            {
                await _context.TbUsers.AddAsync(newUser);
                _context.SaveChangesAsync();
                return RedirectToAction("Index", "Home");
            }
            else return RedirectToAction("Index", "Login");
        }
    }
}
