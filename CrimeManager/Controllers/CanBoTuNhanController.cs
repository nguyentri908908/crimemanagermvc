﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CrimeManager.Models;
using CrimeManager.Helper;

namespace CrimeManager.Controllers
{
    public class CanBoTuNhanController : Controller
    {
        private readonly DbCrimeManageContext _context;

        public CanBoTuNhanController(DbCrimeManageContext context)
        {
            _context = context;
        }

        // GET: CanBoTuNhan
        public async Task<IActionResult> Index()
        {
            var session = HttpContext.Session.GetObject<UserSession>("OpenSession");
            if (session.UserDetail != null)
            {
                if (session.UserDetail.IdUser == 4)
                {
                    ViewBag.CanBo = await _context.TbCanBos.ToListAsync();
                    ViewBag.TuNhan = await _context.TbTuNhans.ToListAsync();
                    var dbCrimeManageContext = _context.TbCanBoTuNhans.Include(t => t.IdCanBoNavigation).Include(t => t.IdTuNhanNavigation);
                    return View(await dbCrimeManageContext.ToListAsync());
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            return View();
            
        }

        // GET: CanBoTuNhan/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.TbCanBoTuNhans == null)
            {
                return NotFound();
            }

            var tbCanBoTuNhan = await _context.TbCanBoTuNhans
                .Include(t => t.IdCanBoNavigation)
                .Include(t => t.IdTuNhanNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tbCanBoTuNhan == null)
            {
                return NotFound();
            }
            ViewData["IdCanBo"] = new SelectList(_context.TbCanBos, "IdCanBo", "TenCanBo", tbCanBoTuNhan.IdCanBo);
            ViewData["IdTuNhan"] = new SelectList(_context.TbTuNhans, "IdTuNhan", "MaTuNhan", tbCanBoTuNhan.IdTuNhan);
            return View(tbCanBoTuNhan);
        }

        // GET: CanBoTuNhan/Create
        public IActionResult Create()
        {
            ViewData["IdCanBo"] = new SelectList(_context.TbCanBos, "IdCanBo", "TenCanBo");
            ViewData["IdTuNhan"] = new SelectList(_context.TbTuNhans, "IdTuNhan", "MaTuNhan");
            return View();
        }

        // POST: CanBoTuNhan/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,IdCanBo,IdTuNhan,ThoiGianBd,ThoiGianKt")] TbCanBoTuNhan tbCanBoTuNhan)
        {
            TbCanBoTuNhan check = await _context.TbCanBoTuNhans.Where(t => t.IdCanBo == tbCanBoTuNhan.IdCanBo && t.IdTuNhan
          == tbCanBoTuNhan.IdTuNhan && t.ThoiGianBd.Value == tbCanBoTuNhan.ThoiGianBd.Value && t.ThoiGianKt.Value == tbCanBoTuNhan.ThoiGianKt.Value).FirstOrDefaultAsync();
            if(check == null)
            {
                _context.Add(tbCanBoTuNhan);
                await _context.SaveChangesAsync();
            }
                return RedirectToAction(nameof(Index));
           
        }

        // GET: CanBoTuNhan/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.TbCanBoTuNhans == null)
            {
                return NotFound();
            }

            var tbCanBoTuNhan = await _context.TbCanBoTuNhans.FindAsync(id);
            if (tbCanBoTuNhan == null)
            {
                return NotFound();
            }
            ViewData["IdCanBo"] = new SelectList(_context.TbCanBos, "IdCanBo", "TenCanBo", tbCanBoTuNhan.IdCanBo);
            ViewData["IdTuNhan"] = new SelectList(_context.TbTuNhans, "IdTuNhan", "MaTuNhan", tbCanBoTuNhan.IdTuNhan);
            return View(tbCanBoTuNhan);
        }

        // POST: CanBoTuNhan/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,IdCanBo,IdTuNhan,ThoiGianBd,ThoiGianKt")] TbCanBoTuNhan tbCanBoTuNhan)
        {
            if (id != tbCanBoTuNhan.Id)
            {
                return NotFound();
            }
                try
                {
                TbCanBoTuNhan check = await _context.TbCanBoTuNhans.Where(t => t.IdCanBo == tbCanBoTuNhan.IdCanBo && t.IdTuNhan
         == tbCanBoTuNhan.IdTuNhan && t.ThoiGianBd.Value == tbCanBoTuNhan.ThoiGianBd.Value && t.ThoiGianKt.Value == tbCanBoTuNhan.ThoiGianKt.Value).FirstOrDefaultAsync();
                if (check == null) {
                    _context.Update(tbCanBoTuNhan);
                    await _context.SaveChangesAsync();
                    }
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TbCanBoTuNhanExists(tbCanBoTuNhan.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
        }

        // GET: CanBoTuNhan/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.TbCanBoTuNhans == null)
            {
                return NotFound();
            }

            var tbCanBoTuNhan = await _context.TbCanBoTuNhans
                .Include(t => t.IdCanBoNavigation)
                .Include(t => t.IdTuNhanNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tbCanBoTuNhan == null)
            {
                return NotFound();
            }
            ViewData["IdCanBo"] = new SelectList(_context.TbCanBos, "IdCanBo", "TenCanBo", tbCanBoTuNhan.IdCanBo);
            ViewData["IdTuNhan"] = new SelectList(_context.TbTuNhans, "IdTuNhan", "MaTuNhan", tbCanBoTuNhan.IdTuNhan);
            return View(tbCanBoTuNhan);
        }

        // POST: CanBoTuNhan/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.TbCanBoTuNhans == null)
            {
                return Problem("Entity set 'DbCrimeManageContext.TbCanBoTuNhans'  is null.");
            }
            var tbCanBoTuNhan = await _context.TbCanBoTuNhans.FindAsync(id);
            if (tbCanBoTuNhan != null)
            {
                _context.TbCanBoTuNhans.Remove(tbCanBoTuNhan);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TbCanBoTuNhanExists(int id)
        {
          return (_context.TbCanBoTuNhans?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
