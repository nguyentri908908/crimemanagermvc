﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CrimeManager.Models;
using CrimeManager.Helper;

namespace CrimeManager.Controllers
{
    public class DiaDiemThamNuoiController : Controller
    {
        private readonly DbCrimeManageContext _context;

        public DiaDiemThamNuoiController(DbCrimeManageContext context)
        {
            _context = context;
        }

        // GET: DiaDiemThamNuoi
        public async Task<IActionResult> Index()
        {
            var session = HttpContext.Session.GetObject<UserSession>("OpenSession");
            if (session.UserDetail != null)
            {
                if (session.UserDetail.IdUser == 4)
                {
                    var dbCrimeManageContext = _context.TbDiaDiemThamNuois;
                    return View(await dbCrimeManageContext.ToListAsync());
                }
                else
                {

                    int idCanBo = session.UserDetail.IdCanBo.Value;
                    var idDiaDiem = _context.TbCanBoDiaDiems.Where(item => item.IdCanBo == session.UserDetail.IdCanBo
                        && DateTime.Now >= item.ThoiGianBd && DateTime.Now <= item.ThoiGianKt).Select(x => x.IdDiaDiem).ToList();
                    var dbCrimeManageContext = _context.TbDiaDiemThamNuois.Where(x => idDiaDiem.Contains(x.IdDiaDiem));
                    return View(await dbCrimeManageContext.ToListAsync());
                }
            }
            return View();
           
        }

        // GET: DiaDiemThamNuoi/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.TbDiaDiemThamNuois == null)
            {
                return NotFound();
            }

            var tbDiaDiemThamNuoi = await _context.TbDiaDiemThamNuois
                .FirstOrDefaultAsync(m => m.IdDiaDiem == id);
            if (tbDiaDiemThamNuoi == null)
            {
                return NotFound();
            }
            List<TbThamNuoi> lstThamNuoi = _context.TbThamNuois.Where(x=>x.IdDiaDiem==id&&x.ThoiGianBd
            >=DateTime.Now).ToList();
            ViewBag.lstThamNuoi = lstThamNuoi;
            return View(tbDiaDiemThamNuoi);
        }

        // GET: DiaDiemThamNuoi/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: DiaDiemThamNuoi/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdDiaDiem,DiaDiem,SoCho")] TbDiaDiemThamNuoi tbDiaDiemThamNuoi)
        {
            if (ModelState.IsValid)
            {
                _context.Add(tbDiaDiemThamNuoi);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(tbDiaDiemThamNuoi);
        }

        // GET: DiaDiemThamNuoi/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.TbDiaDiemThamNuois == null)
            {
                return NotFound();
            }

            var tbDiaDiemThamNuoi = await _context.TbDiaDiemThamNuois.FindAsync(id);
            if (tbDiaDiemThamNuoi == null)
            {
                return NotFound();
            }
            return View(tbDiaDiemThamNuoi);
        }

        // POST: DiaDiemThamNuoi/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdDiaDiem,DiaDiem,SoCho")] TbDiaDiemThamNuoi tbDiaDiemThamNuoi)
        {
            if (id != tbDiaDiemThamNuoi.IdDiaDiem)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(tbDiaDiemThamNuoi);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TbDiaDiemThamNuoiExists(tbDiaDiemThamNuoi.IdDiaDiem))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(tbDiaDiemThamNuoi);
        }

        // GET: DiaDiemThamNuoi/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.TbDiaDiemThamNuois == null)
            {
                return NotFound();
            }

            var tbDiaDiemThamNuoi = await _context.TbDiaDiemThamNuois
                .FirstOrDefaultAsync(m => m.IdDiaDiem == id);
            if (tbDiaDiemThamNuoi == null)
            {
                return NotFound();
            }

            return View(tbDiaDiemThamNuoi);
        }

        // POST: DiaDiemThamNuoi/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.TbDiaDiemThamNuois == null)
            {
                return Problem("Entity set 'DbCrimeManageContext.TbDiaDiemThamNuois'  is null.");
            }
            var tbDiaDiemThamNuoi = await _context.TbDiaDiemThamNuois.FindAsync(id);
            if (tbDiaDiemThamNuoi != null)
            {
                _context.TbDiaDiemThamNuois.Remove(tbDiaDiemThamNuoi);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TbDiaDiemThamNuoiExists(int id)
        {
          return (_context.TbDiaDiemThamNuois?.Any(e => e.IdDiaDiem == id)).GetValueOrDefault();
        }
    }
}
