﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CrimeManager.Models;

namespace CrimeManager.Controllers
{
    public class BanAnController : Controller
    {
        private readonly DbCrimeManageContext _context;

        public BanAnController(DbCrimeManageContext context)
        {
            _context = context;
        }

        // GET: BanAn
        public async Task<IActionResult> Index()
        {
            ViewBag.TuNhan = _context.TbThongTinTuNhans.ToList();
            ViewBag.Loai = _context.TbLoaiBanAns.ToList();
            var dbCrimeManageContext = _context.TbBanAns.Include(t => t.IdPhamNhanNavigation).Include(t => t.LoaiNavigation);
            return View(await dbCrimeManageContext.ToListAsync());
        }

        // GET: BanAn/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.TbBanAns == null)
            {
                return NotFound();
            }

            var tbBanAn = await _context.TbBanAns
                .Include(t => t.IdPhamNhanNavigation)
                .Include(t => t.LoaiNavigation)
                .FirstOrDefaultAsync(m => m.IdBanAn == id);
            if (tbBanAn == null)
            {
                return NotFound();
            }
            ViewData["IdPhamNhan"] = new SelectList(_context.TbThongTinTuNhans, "IdThongTin", "HoTen", tbBanAn.IdPhamNhan);
            ViewData["Loai"] = new SelectList(_context.TbLoaiBanAns, "IdLoai", "Ten", tbBanAn.Loai);
            return View(tbBanAn);
        }

        // GET: BanAn/Create
        public IActionResult Create()
        {
            ViewData["IdPhamNhan"] = new SelectList(_context.TbThongTinTuNhans, "IdThongTin", "HoTen");
            ViewData["Loai"] = new SelectList(_context.TbLoaiBanAns, "IdLoai", "Ten");
            return View();
        }

        // POST: BanAn/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdBanAn,NoiDung,IdPhamNhan,SoNam,NgayBd,Loai,TrangThai")] TbBanAn tbBanAn)
        {
            ViewData["IdPhamNhan"] = new SelectList(_context.TbThongTinTuNhans, "IdThongTin", "HoTen", tbBanAn.IdPhamNhan);
            ViewData["Loai"] = new SelectList(_context.TbLoaiBanAns, "IdLoai", "Ten", tbBanAn.Loai);
            _context.Add(tbBanAn);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        // GET: BanAn/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.TbBanAns == null)
            {
                return NotFound();
            }

            var tbBanAn = await _context.TbBanAns.FindAsync(id);
            if (tbBanAn == null)
            {
                return NotFound();
            }
            ViewData["IdPhamNhan"] = new SelectList(_context.TbThongTinTuNhans, "IdThongTin", "HoTen", tbBanAn.IdPhamNhan);
            ViewData["Loai"] = new SelectList(_context.TbLoaiBanAns, "IdLoai", "Ten", tbBanAn.Loai);
            return View(tbBanAn);
        }

        // POST: BanAn/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdBanAn,NoiDung,IdPhamNhan,SoNam,NgayBd,Loai,TrangThai")] TbBanAn tbBanAn)
        {
            if (id != tbBanAn.IdBanAn)
            {
                return NotFound();
            }


                try
                {
                    _context.Update(tbBanAn);
                    await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TbBanAnExists(tbBanAn.IdBanAn))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                
                
            }
           
        }

        // GET: BanAn/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.TbBanAns == null)
            {
                return NotFound();
            }

            var tbBanAn = await _context.TbBanAns
                .Include(t => t.IdPhamNhanNavigation)
                .Include(t => t.LoaiNavigation)
                .FirstOrDefaultAsync(m => m.IdBanAn == id);
            if (tbBanAn == null)
            {
                return NotFound();
            }
            ViewData["IdPhamNhan"] = new SelectList(_context.TbThongTinTuNhans, "IdThongTin", "HoTen", tbBanAn.IdPhamNhan);
            ViewData["Loai"] = new SelectList(_context.TbLoaiBanAns, "IdLoai", "Ten", tbBanAn.Loai);
            return View(tbBanAn);
        }

        // POST: BanAn/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.TbBanAns == null)
            {
                return Problem("Entity set 'DbCrimeManageContext.TbBanAns'  is null.");
            }
            var tbBanAn = await _context.TbBanAns.FindAsync(id);
            if (tbBanAn != null)
            {
                _context.TbBanAns.Remove(tbBanAn);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TbBanAnExists(int id)
        {
          return (_context.TbBanAns?.Any(e => e.IdBanAn == id)).GetValueOrDefault();
        }
    }
}
