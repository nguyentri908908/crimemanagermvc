﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CrimeManager.Models;
using CrimeManager.Helper;

namespace CrimeManager.Controllers
{
    public class NhomNguoiDungController : Controller
    {
        private readonly DbCrimeManageContext _context;

        public NhomNguoiDungController(DbCrimeManageContext context)
        {
            _context = context;
        }

        // GET: NhomNguoiDung
        public async Task<IActionResult> Index()
        {
            var session = HttpContext.Session.GetObject<UserSession>("OpenSession");
            if (session.UserDetail != null)
            {
                if (session.UserDetail.IdUser == 4)
                {
                    return _context.TbNhomNguoiDungs != null ?
                          View(await _context.TbNhomNguoiDungs.ToListAsync()) :
                          Problem("Entity set 'DbCrimeManageContext.TbNhomNguoiDungs'  is null.");
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            return View();
            
        }

        // GET: NhomNguoiDung/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.TbNhomNguoiDungs == null)
            {
                return NotFound();
            }

            var tbNhomNguoiDung = await _context.TbNhomNguoiDungs
                .FirstOrDefaultAsync(m => m.IdNhom == id);
            if (tbNhomNguoiDung == null)
            {
                return NotFound();
            }
            List<int>lstIdChucNang = _context.TbNhomChucNangs.Where(x=>x.IdNhom==id).Select(item=>item.IdChucNang).ToList();
            List<TbChucNang> lstChucNang = _context.TbChucNangs.Where(x=>lstIdChucNang.Contains(x.IdChucNang)).ToList();
            ViewBag.lstChucNang = lstChucNang;
            return View(tbNhomNguoiDung);
        }

        // GET: NhomNguoiDung/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: NhomNguoiDung/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdNhom,TenNhom")] TbNhomNguoiDung tbNhomNguoiDung)
        {
            if (ModelState.IsValid)
            {
                _context.Add(tbNhomNguoiDung);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(tbNhomNguoiDung);
        }

        // GET: NhomNguoiDung/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.TbNhomNguoiDungs == null)
            {
                return NotFound();
            }

            var tbNhomNguoiDung = await _context.TbNhomNguoiDungs.FindAsync(id);
            if (tbNhomNguoiDung == null)
            {
                return NotFound();
            }
            return View(tbNhomNguoiDung);
        }

        // POST: NhomNguoiDung/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdNhom,TenNhom")] TbNhomNguoiDung tbNhomNguoiDung)
        {
            if (id != tbNhomNguoiDung.IdNhom)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(tbNhomNguoiDung);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TbNhomNguoiDungExists(tbNhomNguoiDung.IdNhom))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(tbNhomNguoiDung);
        }

        // GET: NhomNguoiDung/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.TbNhomNguoiDungs == null)
            {
                return NotFound();
            }

            var tbNhomNguoiDung = await _context.TbNhomNguoiDungs
                .FirstOrDefaultAsync(m => m.IdNhom == id);
            if (tbNhomNguoiDung == null)
            {
                return NotFound();
            }

            return View(tbNhomNguoiDung);
        }

        // POST: NhomNguoiDung/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.TbNhomNguoiDungs == null)
            {
                return Problem("Entity set 'DbCrimeManageContext.TbNhomNguoiDungs'  is null.");
            }
            var tbNhomNguoiDung = await _context.TbNhomNguoiDungs.FindAsync(id);
            if (tbNhomNguoiDung != null)
            {
                _context.TbNhomNguoiDungs.Remove(tbNhomNguoiDung);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TbNhomNguoiDungExists(int id)
        {
          return (_context.TbNhomNguoiDungs?.Any(e => e.IdNhom == id)).GetValueOrDefault();
        }
    }
}
