﻿using Microsoft.AspNetCore.Mvc;
namespace CrimeManager.Controllers
{
    public class PartialController : Controller
    {
        
        public ActionResult Sidebar()
        {
            // Trả về partial view của sidebar
            return PartialView("partials/sidebar");
        }
    }
}
