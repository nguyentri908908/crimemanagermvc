﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CrimeManager.Models;

namespace CrimeManager.Controllers
{
    public class CapBacController : Controller
    {
        private readonly DbCrimeManageContext _context;

        public CapBacController(DbCrimeManageContext context)
        {
            _context = context;
        }

        // GET: CapBac
        public async Task<IActionResult> Index()
        {
              return _context.TbCapBacs != null ? 
                          View(await _context.TbCapBacs.ToListAsync()) :
                          Problem("Entity set 'DbCrimeManageContext.TbCapBacs'  is null.");
        }

        // GET: CapBac/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.TbCapBacs == null)
            {
                return NotFound();
            }

            var tbCapBac = await _context.TbCapBacs
                .FirstOrDefaultAsync(m => m.IdCapBac == id);
            if (tbCapBac == null)
            {
                return NotFound();
            }

            return View(tbCapBac);
        }

        // GET: CapBac/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: CapBac/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdCapBac,TenCapBac")] TbCapBac tbCapBac)
        {
            if (ModelState.IsValid)
            {
                _context.Add(tbCapBac);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(tbCapBac);
        }

        // GET: CapBac/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.TbCapBacs == null)
            {
                return NotFound();
            }

            var tbCapBac = await _context.TbCapBacs.FindAsync(id);
            if (tbCapBac == null)
            {
                return NotFound();
            }
            return View(tbCapBac);
        }

        // POST: CapBac/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdCapBac,TenCapBac")] TbCapBac tbCapBac)
        {
            if (id != tbCapBac.IdCapBac)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(tbCapBac);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TbCapBacExists(tbCapBac.IdCapBac))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(tbCapBac);
        }

        // GET: CapBac/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.TbCapBacs == null)
            {
                return NotFound();
            }

            var tbCapBac = await _context.TbCapBacs
                .FirstOrDefaultAsync(m => m.IdCapBac == id);
            if (tbCapBac == null)
            {
                return NotFound();
            }

            return View(tbCapBac);
        }

        // POST: CapBac/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.TbCapBacs == null)
            {
                return Problem("Entity set 'DbCrimeManageContext.TbCapBacs'  is null.");
            }
            var tbCapBac = await _context.TbCapBacs.FindAsync(id);
            if (tbCapBac != null)
            {
                _context.TbCapBacs.Remove(tbCapBac);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TbCapBacExists(int id)
        {
          return (_context.TbCapBacs?.Any(e => e.IdCapBac == id)).GetValueOrDefault();
        }
    }
}
