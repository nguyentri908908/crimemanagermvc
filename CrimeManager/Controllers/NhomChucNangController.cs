﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CrimeManager.Models;
using CrimeManager.Helper;

namespace CrimeManager.Controllers
{
    public class NhomChucNangController : Controller
    {
        private readonly DbCrimeManageContext _context;

        public NhomChucNangController(DbCrimeManageContext context)
        {
            _context = context;
        }

        // GET: NhomChucNang
        public async Task<IActionResult> Index()
        {
            var session = HttpContext.Session.GetObject<UserSession>("OpenSession");
            if (session.UserDetail != null)
            {
                if (session.UserDetail.IdUser == 4)
                {
                    ViewBag.Nhom = await _context.TbNhomNguoiDungs.ToListAsync();
                    ViewBag.ChucNang = await _context.TbChucNangs.ToListAsync();
                    var dbCrimeManageContext = _context.TbNhomChucNangs.Include(t => t.IdChucNangNavigation).Include(t => t.IdNhomNavigation);
                    return View(await dbCrimeManageContext.ToListAsync());
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            return View();
           
        }

        // GET: NhomChucNang/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.TbNhomChucNangs == null)
            {
                return NotFound();
            }

            var tbNhomChucNang = await _context.TbNhomChucNangs
                .Include(t => t.IdChucNangNavigation)
                .Include(t => t.IdNhomNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tbNhomChucNang == null)
            {
                return NotFound();
            }
            ViewData["IdChucNang"] = new SelectList(_context.TbChucNangs, "IdChucNang", "TenChucNang", tbNhomChucNang.IdChucNang);
            ViewData["IdNhom"] = new SelectList(_context.TbNhomNguoiDungs, "IdNhom", "TenNhom", tbNhomChucNang.IdNhom);
            return View(tbNhomChucNang);
        }

        // GET: NhomChucNang/Create
        public IActionResult Create()
        {
            ViewData["IdChucNang"] = new SelectList(_context.TbChucNangs, "IdChucNang", "TenChucNang");
            ViewData["IdNhom"] = new SelectList(_context.TbNhomNguoiDungs, "IdNhom", "TenNhom");
            return View();
        }

        // POST: NhomChucNang/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,IdNhom,IdChucNang")] TbNhomChucNang tbNhomChucNang)
        {

                _context.Add(tbNhomChucNang);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            
        }

        // GET: NhomChucNang/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.TbNhomChucNangs == null)
            {
                return NotFound();
            }

            var tbNhomChucNang = await _context.TbNhomChucNangs.FindAsync(id);
            if (tbNhomChucNang == null)
            {
                return NotFound();
            }
            ViewData["IdChucNang"] = new SelectList(_context.TbChucNangs, "IdChucNang", "TenChucNang", tbNhomChucNang.IdChucNang);
            ViewData["IdNhom"] = new SelectList(_context.TbNhomNguoiDungs, "IdNhom", "TenNhom", tbNhomChucNang.IdNhom);
            return View(tbNhomChucNang);
        }

        // POST: NhomChucNang/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,IdNhom,IdChucNang")] TbNhomChucNang tbNhomChucNang)
        {
            if (id != tbNhomChucNang.Id)
            {
                return NotFound();
            }

            
                try
                {
                    _context.Update(tbNhomChucNang);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TbNhomChucNangExists(tbNhomChucNang.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
           
        }

        // GET: NhomChucNang/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.TbNhomChucNangs == null)
            {
                return NotFound();
            }

            var tbNhomChucNang = await _context.TbNhomChucNangs
                .Include(t => t.IdChucNangNavigation)
                .Include(t => t.IdNhomNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tbNhomChucNang == null)
            {
                return NotFound();
            }
            ViewData["IdChucNang"] = new SelectList(_context.TbChucNangs, "IdChucNang", "TenChucNang", tbNhomChucNang.IdChucNang);
            ViewData["IdNhom"] = new SelectList(_context.TbNhomNguoiDungs, "IdNhom", "TenNhom", tbNhomChucNang.IdNhom);
            return View(tbNhomChucNang);
        }

        // POST: NhomChucNang/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.TbNhomChucNangs == null)
            {
                return Problem("Entity set 'DbCrimeManageContext.TbNhomChucNangs'  is null.");
            }
            var tbNhomChucNang = await _context.TbNhomChucNangs.FindAsync(id);
            if (tbNhomChucNang != null)
            {
                _context.TbNhomChucNangs.Remove(tbNhomChucNang);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TbNhomChucNangExists(int id)
        {
          return (_context.TbNhomChucNangs?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
