﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CrimeManager.Models;
using System.Text;
using System.Security.Cryptography;
using CrimeManager.Helper;

namespace CrimeManager.Controllers
{
    public class UserController : Controller
    {
        private readonly DbCrimeManageContext _context;

        public UserController(DbCrimeManageContext context)
        {
            _context = context;
        }
        static string ComputeSHA256Hash(string input)
        {
            using (SHA256 sha256 = SHA256.Create())
            {
                // Chuyển đổi chuỗi thành mảng byte
                byte[] inputBytes = Encoding.UTF8.GetBytes(input);

                // Tính toán giá trị băm cho mảng byte
                byte[] hashBytes = sha256.ComputeHash(inputBytes);

                // Chuyển đổi mảng byte thành chuỗi hex
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    builder.Append(hashBytes[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }
        // GET: User
        public async Task<IActionResult> Index()
        {
            var session = HttpContext.Session.GetObject<UserSession>("OpenSession");
            if (session.UserDetail != null)
            {
                if (session.UserDetail.IdUser == 4)
                {
                    ViewBag.CanBo = await _context.TbCanBos.ToListAsync();
                    var dbCrimeManageContext = _context.TbUsers.Include(t => t.IdCanBoNavigation);
                    return View(await dbCrimeManageContext.ToListAsync());
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            return View();
            
        }

        // GET: User/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.TbUsers == null)
            {
                return NotFound();
            }

            var tbUser = await _context.TbUsers
                .Include(t => t.IdCanBoNavigation)
                .FirstOrDefaultAsync(m => m.IdUser == id);
            if (tbUser == null)
            {
                return NotFound();
            }
            ViewData["IdCanBo"] = new SelectList(_context.TbCanBos, "IdCanBo", "TenCanBo", tbUser.IdCanBo);
            return View(tbUser);
        }

        // GET: User/Create
        public IActionResult Create()
        {
            List<int?> lstCanBoDaCo = _context.TbUsers.Select(t=> t.IdCanBo).ToList();
            ViewData["IdCanBo"] = new SelectList(_context.TbCanBos.Where(x=>!lstCanBoDaCo.Contains(x.IdCanBo)
            &&x.IdCanBo!=4), "IdCanBo", "TenCanBo");
            return View();
        }

        // POST: User/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdUser,IdCanBo,UserName,Password")] TbUser tbUser)
        {
                tbUser.Password = ComputeSHA256Hash(tbUser.Password);
                _context.Add(tbUser);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            
           
        }

        // GET: User/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.TbUsers == null)
            {
                return NotFound();
            }

            var tbUser = await _context.TbUsers.FindAsync(id);
            if (tbUser == null)
            {
                return NotFound();
            }
            ViewData["IdCanBo"] = new SelectList(_context.TbCanBos, "IdCanBo", "TenCanBo", tbUser.IdCanBo);
            return View(tbUser);
        }

        // POST: User/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdUser,IdCanBo,UserName,Password")] TbUser tbUser)
        {
            if (id != tbUser.IdUser)
            {
                return NotFound();
            }

            
                try
                {
                tbUser.Password = ComputeSHA256Hash(tbUser.Password);
                _context.Update(tbUser);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TbUserExists(tbUser.IdUser))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            
            
        }

        // GET: User/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.TbUsers == null)
            {
                return NotFound();
            }

            var tbUser = await _context.TbUsers
                .Include(t => t.IdCanBoNavigation)
                .FirstOrDefaultAsync(m => m.IdUser == id);
            if (tbUser == null)
            {
                return NotFound();
            }
            ViewData["IdCanBo"] = new SelectList(_context.TbCanBos, "IdCanBo", "TenCanBo", tbUser.IdCanBo);
            return View(tbUser);
        }

        // POST: User/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.TbUsers == null)
            {
                return Problem("Entity set 'DbCrimeManageContext.TbUsers'  is null.");
            }
            var tbUser = await _context.TbUsers.FindAsync(id);
            if (tbUser != null)
            {
                _context.TbUsers.Remove(tbUser);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TbUserExists(int id)
        {
          return (_context.TbUsers?.Any(e => e.IdUser == id)).GetValueOrDefault();
        }
    }
}
