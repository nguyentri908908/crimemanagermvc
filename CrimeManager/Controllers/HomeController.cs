﻿using CrimeManager.Helper;
using CrimeManager.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace CrimeManager.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly DbCrimeManageContext _context;

        public HomeController(ILogger<HomeController> logger, DbCrimeManageContext context)
        {
            _logger = logger;
            _context = context;
        }

        public IActionResult Index()
        {
            // Lấy dữ liệu từ session
            var session = HttpContext.Session.GetObject<UserSession>("OpenSession");

            if (session != null && session.UserDetail != null)
            {
                if (session.UserDetail.IdUser != 0)
                {
                    if(session.UserDetail.IdUser == 4)
                    {
                        ViewBag.ThamNuoi = _context.TbThamNuois.Count();
                        ViewBag.PhamNhan = _context.TbTuNhans.Count();
                        ViewBag.PhongGiam = _context.TbPhongGiams.Count();
                    }
                    else 
                    {
                        List<int> diaDiem = _context.TbCanBoDiaDiems.Where(item => item.IdCanBo
                        == session.UserDetail.IdCanBo && item.ThoiGianBd.Value.Date == DateTime.Today).
                        Select(x => x.IdDiaDiem.Value).ToList();
                        ViewBag.ThamNuoi = _context.TbThamNuois.Where(item=>diaDiem.Contains(item.IdDiaDiem.Value)
                        && item.ThoiGianBd.Value.Date == DateTime.Now).Count();
                        ViewBag.PhamNhan = _context.TbCanBoTuNhans.Where(item=>item.IdCanBo== session.UserDetail.IdCanBo
                        && DateTime.Now>=item.ThoiGianBd&&DateTime.Now<=item.ThoiGianKt).Count();
                        ViewBag.PhongGiam = _context.TbCanBoPhongGiams.Where(item => item.IdCanBo == session.UserDetail.IdCanBo
                        && DateTime.Now >= item.ThoiGianBd && DateTime.Now <= item.ThoiGianKt).Count();
                    }
                    // Nếu IdUser khác 0, tức là người dùng đã đăng nhập
                    return View();
                }
            }

            // Nếu session hoặc userDetail không tồn tại hoặc IdUser == 0, chuyển hướng về trang đăng nhập
            return RedirectToAction("Index", "Login");
        }
        public String GetUserName()
        {
            var session = HttpContext.Session.GetObject<UserSession>("OpenSession");
            return session.UserDetail.UserName; // Of whatever you need to return.
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}