﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CrimeManager.Models;

namespace CrimeManager.Controllers
{
    public class LoaiBanAnController : Controller
    {
        private readonly DbCrimeManageContext _context;

        public LoaiBanAnController(DbCrimeManageContext context)
        {
            _context = context;
        }

        // GET: LoaiBanAn
        public async Task<IActionResult> Index()
        {
              return _context.TbLoaiBanAns != null ? 
                          View(await _context.TbLoaiBanAns.ToListAsync()) :
                          Problem("Entity set 'DbCrimeManageContext.TbLoaiBanAns'  is null.");
        }

        // GET: LoaiBanAn/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.TbLoaiBanAns == null)
            {
                return NotFound();
            }

            var tbLoaiBanAn = await _context.TbLoaiBanAns
                .FirstOrDefaultAsync(m => m.IdLoai == id);
            if (tbLoaiBanAn == null)
            {
                return NotFound();
            }

            return View(tbLoaiBanAn);
        }

        // GET: LoaiBanAn/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: LoaiBanAn/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdLoai,Ten")] TbLoaiBanAn tbLoaiBanAn)
        {
            if (ModelState.IsValid)
            {
                _context.Add(tbLoaiBanAn);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(tbLoaiBanAn);
        }

        // GET: LoaiBanAn/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.TbLoaiBanAns == null)
            {
                return NotFound();
            }

            var tbLoaiBanAn = await _context.TbLoaiBanAns.FindAsync(id);
            if (tbLoaiBanAn == null)
            {
                return NotFound();
            }
            return View(tbLoaiBanAn);
        }

        // POST: LoaiBanAn/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdLoai,Ten")] TbLoaiBanAn tbLoaiBanAn)
        {
            if (id != tbLoaiBanAn.IdLoai)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(tbLoaiBanAn);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TbLoaiBanAnExists(tbLoaiBanAn.IdLoai))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(tbLoaiBanAn);
        }

        // GET: LoaiBanAn/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.TbLoaiBanAns == null)
            {
                return NotFound();
            }

            var tbLoaiBanAn = await _context.TbLoaiBanAns
                .FirstOrDefaultAsync(m => m.IdLoai == id);
            if (tbLoaiBanAn == null)
            {
                return NotFound();
            }

            return View(tbLoaiBanAn);
        }

        // POST: LoaiBanAn/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.TbLoaiBanAns == null)
            {
                return Problem("Entity set 'DbCrimeManageContext.TbLoaiBanAns'  is null.");
            }
            var tbLoaiBanAn = await _context.TbLoaiBanAns.FindAsync(id);
            if (tbLoaiBanAn != null)
            {
                _context.TbLoaiBanAns.Remove(tbLoaiBanAn);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TbLoaiBanAnExists(int id)
        {
          return (_context.TbLoaiBanAns?.Any(e => e.IdLoai == id)).GetValueOrDefault();
        }
    }
}
