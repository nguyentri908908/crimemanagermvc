﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CrimeManager.Models;
using CrimeManager.Helper;

namespace CrimeManager.Controllers
{
    public class CanBoController : Controller
    {
        private readonly DbCrimeManageContext _context;

        public CanBoController(DbCrimeManageContext context)
        {
            _context = context;
        }

        // GET: CanBo
        public async Task<IActionResult> Index()
        {
            var session = HttpContext.Session.GetObject<UserSession>("OpenSession");
            if (session.UserDetail != null)
            {
                if (session.UserDetail.IdUser == 4)
                {
                    ViewBag.CapBac = await _context.TbCapBacs.ToListAsync();
                    var dbCrimeManageContext = _context.TbCanBos.Include(t => t.CapBacNavigation);
                    return View(await dbCrimeManageContext.ToListAsync());
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            return View();
            
        }

        // GET: CanBo/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.TbCanBos == null)
            {
                return NotFound();
            }

            var tbCanBo = await _context.TbCanBos
                .Include(t => t.CapBacNavigation)
                .FirstOrDefaultAsync(m => m.IdCanBo == id);
            if (tbCanBo == null)
            {
                return NotFound();
            }
            ViewData["CapBac"] = new SelectList(_context.TbCapBacs, "IdCapBac", "TenCapBac",tbCanBo.CapBac);
            List<int>lstId = _context.TbCanBoNhoms.Where(x=>x.IdCanBo==id).Select(x => x.IdNhom).ToList();
            List<TbNhomNguoiDung> lstNhom = _context.TbNhomNguoiDungs.Where(x=>lstId.Contains(x.IdNhom)).ToList();
            ViewBag.lstNhom = lstNhom;
            return View(tbCanBo);
        }

        // GET: CanBo/Create
        public IActionResult Create()
        {
            ViewData["CapBac"] = new SelectList(_context.TbCapBacs, "IdCapBac", "TenCapBac");
            return View();
        }

        // POST: CanBo/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdCanBo,TenCanBo,SoHieuCanBo,Sdt,SinhNhat,CapBac")] TbCanBo tbCanBo)
        {

                _context.Add(tbCanBo);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            
        }

        // GET: CanBo/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.TbCanBos == null)
            {
                return NotFound();
            }

            var tbCanBo = await _context.TbCanBos.FindAsync(id);
            if (tbCanBo == null)
            {
                return NotFound();
            }
            ViewData["CapBac"] = new SelectList(_context.TbCapBacs, "IdCapBac", "TenCapBac", tbCanBo.CapBac);
            return View(tbCanBo);
        }

        // POST: CanBo/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdCanBo,TenCanBo,SoHieuCanBo,Sdt,SinhNhat,CapBac")] TbCanBo tbCanBo)
        {
            if (id != tbCanBo.IdCanBo)
            {
                return NotFound();
            }


                try
                {
                    _context.Update(tbCanBo);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TbCanBoExists(tbCanBo.IdCanBo))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
       
        }

        // GET: CanBo/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.TbCanBos == null)
            {
                return NotFound();
            }

            var tbCanBo = await _context.TbCanBos
                .Include(t => t.CapBacNavigation)
                .FirstOrDefaultAsync(m => m.IdCanBo == id);
            if (tbCanBo == null)
            {
                return NotFound();
            }
            ViewData["CapBac"] = new SelectList(_context.TbCapBacs, "IdCapBac", "TenCapBac", tbCanBo.CapBac);
            return View(tbCanBo);
        }

        // POST: CanBo/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.TbCanBos == null)
            {
                return Problem("Entity set 'DbCrimeManageContext.TbCanBos'  is null.");
            }
            var tbCanBo = await _context.TbCanBos.FindAsync(id);
            if (tbCanBo != null)
            {
                _context.TbCanBos.Remove(tbCanBo);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TbCanBoExists(int id)
        {
          return (_context.TbCanBos?.Any(e => e.IdCanBo == id)).GetValueOrDefault();
        }
    }
}
