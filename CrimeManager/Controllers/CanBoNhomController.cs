﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CrimeManager.Models;
using CrimeManager.Helper;

namespace CrimeManager.Controllers
{
    public class CanBoNhomController : Controller
    {
        private readonly DbCrimeManageContext _context;

        public CanBoNhomController(DbCrimeManageContext context)
        {
            _context = context;
        }

        // GET: CanBoNhom
        public async Task<IActionResult> Index()
        {
            var session = HttpContext.Session.GetObject<UserSession>("OpenSession");
            if (session.UserDetail != null)
            {
                if (session.UserDetail.IdUser == 4)
                {
                    ViewBag.CanBo = await _context.TbCanBos.ToListAsync();
                    ViewBag.Nhom = await _context.TbNhomNguoiDungs.ToListAsync();
                    var dbCrimeManageContext = _context.TbCanBoNhoms.Include(t => t.IdCanBoNavigation).Include(t => t.IdNhomNavigation);
                    return View(await dbCrimeManageContext.ToListAsync());
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            return View();
            
        }

        // GET: CanBoNhom/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.TbCanBoNhoms == null)
            {
                return NotFound();
            }

            var tbCanBoNhom = await _context.TbCanBoNhoms
                .Include(t => t.IdCanBoNavigation)
                .Include(t => t.IdNhomNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tbCanBoNhom == null)
            {
                return NotFound();
            }
            ViewData["IdCanBo"] = new SelectList(_context.TbCanBos, "IdCanBo", "TenCanBo", tbCanBoNhom.IdCanBo);
            ViewData["IdNhom"] = new SelectList(_context.TbNhomNguoiDungs, "IdNhom", "TenNhom", tbCanBoNhom.IdNhom);
            return View(tbCanBoNhom);
        }

        // GET: CanBoNhom/Create
        public IActionResult Create()
        {
            ViewData["IdCanBo"] = new SelectList(_context.TbCanBos, "IdCanBo", "TenCanBo");
            ViewData["IdNhom"] = new SelectList(_context.TbNhomNguoiDungs, "IdNhom", "TenNhom");
            return View();
        }

        // POST: CanBoNhom/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,IdCanBo,IdNhom")] TbCanBoNhom tbCanBoNhom)
        {
            TbCanBoNhom check = await _context.TbCanBoNhoms.Where(x=>x.IdCanBo==tbCanBoNhom.IdCanBo&&x.IdNhom==tbCanBoNhom.IdNhom)
                .FirstOrDefaultAsync();
            if (check == null)
            {
                await _context.AddAsync(tbCanBoNhom);
                await _context.SaveChangesAsync();
            }
                return RedirectToAction(nameof(Index));   
        }

        // GET: CanBoNhom/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.TbCanBoNhoms == null)
            {
                return NotFound();
            }

            var tbCanBoNhom = await _context.TbCanBoNhoms.FindAsync(id);
            if (tbCanBoNhom == null)
            {
                return NotFound();
            }
            ViewData["IdCanBo"] = new SelectList(_context.TbCanBos, "IdCanBo", "TenCanBo", tbCanBoNhom.IdCanBo);
            ViewData["IdNhom"] = new SelectList(_context.TbNhomNguoiDungs, "IdNhom", "TenNhom", tbCanBoNhom.IdNhom);
            return View(tbCanBoNhom);
        }

        // POST: CanBoNhom/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,IdCanBo,IdNhom")] TbCanBoNhom tbCanBoNhom)
        {
            if (id != tbCanBoNhom.Id)
            {
                return NotFound();
            }
                try
                {
                TbCanBoNhom check = await _context.TbCanBoNhoms.Where(x => x.IdCanBo == tbCanBoNhom.IdCanBo && x.IdNhom == tbCanBoNhom.IdNhom)
    .FirstOrDefaultAsync();
                if (check == null)
                {
                    _context.Update(tbCanBoNhom);
                    await _context.SaveChangesAsync();
                }

                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TbCanBoNhomExists(tbCanBoNhom.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
           
        }

        // GET: CanBoNhom/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.TbCanBoNhoms == null)
            {
                return NotFound();
            }

            var tbCanBoNhom = await _context.TbCanBoNhoms
                .Include(t => t.IdCanBoNavigation)
                .Include(t => t.IdNhomNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tbCanBoNhom == null)
            {
                return NotFound();
            }
            ViewData["IdCanBo"] = new SelectList(_context.TbCanBos, "IdCanBo", "TenCanBo", tbCanBoNhom.IdCanBo);
            ViewData["IdNhom"] = new SelectList(_context.TbNhomNguoiDungs, "IdNhom", "TenNhom", tbCanBoNhom.IdNhom);
            return View(tbCanBoNhom);
        }

        // POST: CanBoNhom/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.TbCanBoNhoms == null)
            {
                return Problem("Entity set 'DbCrimeManageContext.TbCanBoNhoms'  is null.");
            }
            var tbCanBoNhom = await _context.TbCanBoNhoms.FindAsync(id);
            if (tbCanBoNhom != null)
            {
                _context.TbCanBoNhoms.Remove(tbCanBoNhom);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TbCanBoNhomExists(int id)
        {
          return (_context.TbCanBoNhoms?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
