﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CrimeManager.Models;
using CrimeManager.Helper;

namespace CrimeManager.Controllers
{
    public class TbChucNangsController : Controller
    {
        private readonly DbCrimeManageContext _context;

        public TbChucNangsController(DbCrimeManageContext context)
        {
            _context = context;
        }

        // GET: TbChucNangs
        public async Task<IActionResult> Index()
        {
            var session = HttpContext.Session.GetObject<UserSession>("OpenSession");
            if (session.UserDetail != null)
            {
                if (session.UserDetail.IdUser == 4)
                {
                    ViewBag.ChucNangCha = _context.TbChucNangs.ToList();
                    return _context.TbChucNangs != null ?
                                  View(await _context.TbChucNangs.ToListAsync()) :
                                  Problem("Entity set 'DbCrimeManageContext.TbChucNangs'  is null.");
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            return View();
           
        }

        // GET: TbChucNangs/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.TbChucNangs == null)
            {
                return NotFound();
            }

            var tbChucNang = await _context.TbChucNangs
                .FirstOrDefaultAsync(m => m.IdChucNang == id);
            if (tbChucNang == null)
            {
                return NotFound();
            }
            var chucNangs = _context.TbChucNangs.ToList();
            chucNangs.Insert(0, new TbChucNang { IdChucNang = 0, TenChucNang = "Chọn chức năng cha" });
            ViewData["IdChucNangCha"] = new SelectList(chucNangs, "IdChucNang", "TenChucNang",tbChucNang.IdChucNangCha);
            return View(tbChucNang);
        }

        // GET: TbChucNangs/Create
        public IActionResult Create()
        {
            var chucNangs = _context.TbChucNangs.ToList();
            chucNangs.Insert(0, new TbChucNang { IdChucNang = 0, TenChucNang = "Chọn chức năng cha" });
            ViewData["IdChucNangCha"] = new SelectList(chucNangs, "IdChucNang", "TenChucNang");           
            return View();
        }

        // POST: TbChucNangs/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdChucNang,IdChucNangCha,TenChucNang,DuongDan,Icon")] TbChucNang tbChucNang)
        {
            if (ModelState.IsValid)
            {
                _context.Add(tbChucNang);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(tbChucNang);
        }

        // GET: TbChucNangs/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.TbChucNangs == null)
            {
                return NotFound();
            }

            var tbChucNang = await _context.TbChucNangs.FindAsync(id);
            if (tbChucNang == null)
            {
                return NotFound();
            }
            var chucNangs = _context.TbChucNangs.ToList();
            chucNangs.Insert(0, new TbChucNang { IdChucNang = 0, TenChucNang = "Chọn chức năng cha" });
            ViewData["IdChucNangCha"] = new SelectList(chucNangs, "IdChucNang", "TenChucNang", tbChucNang.IdChucNangCha);
            return View(tbChucNang);
        }

        // POST: TbChucNangs/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdChucNang,IdChucNangCha,TenChucNang,DuongDan,Icon")] TbChucNang tbChucNang)
        {
            if (id != tbChucNang.IdChucNang)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(tbChucNang);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TbChucNangExists(tbChucNang.IdChucNang))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(tbChucNang);
        }

        // GET: TbChucNangs/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.TbChucNangs == null)
            {
                return NotFound();
            }

            var tbChucNang = await _context.TbChucNangs
                .FirstOrDefaultAsync(m => m.IdChucNang == id);
            if (tbChucNang == null)
            {
                return NotFound();
            }

            return View(tbChucNang);
        }

        // POST: TbChucNangs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.TbChucNangs == null)
            {
                return Problem("Entity set 'DbCrimeManageContext.TbChucNangs'  is null.");
            }
            var tbChucNang = await _context.TbChucNangs.FindAsync(id);
            int k = 1;
            if (tbChucNang != null)
            {
                _context.TbChucNangs.Remove(tbChucNang);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TbChucNangExists(int id)
        {
          return (_context.TbChucNangs?.Any(e => e.IdChucNang == id)).GetValueOrDefault();
        }
    }
}
