﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CrimeManager.Models;
using CrimeManager.Helper;

namespace CrimeManager.Controllers
{
    public class PhongGiamController : Controller
    {
        private readonly DbCrimeManageContext _context;

        public PhongGiamController(DbCrimeManageContext context)
        {
            _context = context;
        }

        // GET: PhongGiam
        public async Task<IActionResult> Index()
        {
            var session = HttpContext.Session.GetObject<UserSession>("OpenSession");
            if (session.UserDetail != null)
            {
                if (session.UserDetail.IdUser == 4)
                {
                    var dbCrimeManageContext = _context.TbPhongGiams;
                    return View(await dbCrimeManageContext.ToListAsync());
                }
                else
                {

                    int idCanBo = session.UserDetail.IdCanBo.Value;
                    var idPhongGiam = _context.TbCanBoPhongGiams.Where(item => item.IdCanBo == session.UserDetail.IdCanBo
                        && DateTime.Now >= item.ThoiGianBd && DateTime.Now <= item.ThoiGianKt).Select(x => x.IdPhongGiam).ToList();
                    var dbCrimeManageContext = _context.TbPhongGiams.Where(x => idPhongGiam.Contains(x.IdPhongGiam));
                    return View(await dbCrimeManageContext.ToListAsync());
                }
            }
            return View();
        }

        // GET: PhongGiam/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.TbPhongGiams == null)
            {
                return NotFound();
            }

            var tbPhongGiam = await _context.TbPhongGiams
                .FirstOrDefaultAsync(m => m.IdPhongGiam == id);
            if (tbPhongGiam == null)
            {
                return NotFound();
            }
            List<TbTuNhan> lstTuNhan = _context.TbTuNhans.Where(x => x.IdPhongGiam == id && 
            x.NgayRaTu>=DateTime.Now).ToList();
            ViewBag.lstTuNhan = lstTuNhan;
            return View(tbPhongGiam);
        }

        // GET: PhongGiam/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: PhongGiam/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdPhongGiam,TenPhong,SoCho,TrangThai")] TbPhongGiam tbPhongGiam)
        {
            if (ModelState.IsValid)
            {
                _context.Add(tbPhongGiam);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(tbPhongGiam);
        }

        // GET: PhongGiam/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.TbPhongGiams == null)
            {
                return NotFound();
            }

            var tbPhongGiam = await _context.TbPhongGiams.FindAsync(id);
            if (tbPhongGiam == null)
            {
                return NotFound();
            }
            return View(tbPhongGiam);
        }

        // POST: PhongGiam/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdPhongGiam,TenPhong,SoCho,TrangThai")] TbPhongGiam tbPhongGiam)
        {
            if (id != tbPhongGiam.IdPhongGiam)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(tbPhongGiam);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TbPhongGiamExists(tbPhongGiam.IdPhongGiam))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(tbPhongGiam);
        }

        // GET: PhongGiam/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.TbPhongGiams == null)
            {
                return NotFound();
            }

            var tbPhongGiam = await _context.TbPhongGiams
                .FirstOrDefaultAsync(m => m.IdPhongGiam == id);
            if (tbPhongGiam == null)
            {
                return NotFound();
            }

            return View(tbPhongGiam);
        }

        // POST: PhongGiam/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.TbPhongGiams == null)
            {
                return Problem("Entity set 'DbCrimeManageContext.TbPhongGiams'  is null.");
            }
            var tbPhongGiam = await _context.TbPhongGiams.FindAsync(id);
            if (tbPhongGiam != null)
            {
                _context.TbPhongGiams.Remove(tbPhongGiam);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TbPhongGiamExists(int id)
        {
          return (_context.TbPhongGiams?.Any(e => e.IdPhongGiam == id)).GetValueOrDefault();
        }
    }
}
