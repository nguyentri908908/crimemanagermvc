﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CrimeManager.Models;
using CrimeManager.Helper;

namespace CrimeManager.Controllers
{
    public class CanBoDiaDiemController : Controller
    {
        private readonly DbCrimeManageContext _context;

        public CanBoDiaDiemController(DbCrimeManageContext context)
        {
            _context = context;
        }

        // GET: CanBoDiaDiem
        public async Task<IActionResult> Index()
        {
            var session = HttpContext.Session.GetObject<UserSession>("OpenSession");
            if (session.UserDetail != null)
            {
                if (session.UserDetail.IdUser == 4)
                {
                    ViewBag.CanBo = await _context.TbCanBos.ToListAsync();
                    ViewBag.DiaDiem = await _context.TbDiaDiemThamNuois.ToListAsync();
                    var dbCrimeManageContext = _context.TbCanBoDiaDiems.Include(t => t.IdCanBoNavigation).Include(t => t.IdDiaDiemNavigation);
                    return View(await dbCrimeManageContext.ToListAsync());
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            return View();
            
        }

        // GET: CanBoDiaDiem/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.TbCanBoDiaDiems == null)
            {
                return NotFound();
            }

            var tbCanBoDiaDiem = await _context.TbCanBoDiaDiems
                .Include(t => t.IdCanBoNavigation)
                .Include(t => t.IdDiaDiemNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tbCanBoDiaDiem == null)
            {
                return NotFound();
            }
            ViewData["IdCanBo"] = new SelectList(_context.TbCanBos, "IdCanBo", "TenCanBo", tbCanBoDiaDiem.IdCanBo);
            ViewData["IdDiaDiem"] = new SelectList(_context.TbDiaDiemThamNuois, "IdDiaDiem", "DiaDiem", tbCanBoDiaDiem.IdDiaDiem);
            return View(tbCanBoDiaDiem);
        }

        // GET: CanBoDiaDiem/Create
        public IActionResult Create()
        {
            ViewData["IdCanBo"] = new SelectList(_context.TbCanBos, "IdCanBo", "TenCanBo");
            ViewData["IdDiaDiem"] = new SelectList(_context.TbDiaDiemThamNuois, "IdDiaDiem", "DiaDiem");
            return View();
        }

        // POST: CanBoDiaDiem/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,IdCanBo,IdDiaDiem,ThoiGianBd,ThoiGianKt")] TbCanBoDiaDiem tbCanBoDiaDiem)
        {
           
                _context.Add(tbCanBoDiaDiem);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            
            
        }

        // GET: CanBoDiaDiem/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.TbCanBoDiaDiems == null)
            {
                return NotFound();
            }

            var tbCanBoDiaDiem = await _context.TbCanBoDiaDiems.FindAsync(id);
            if (tbCanBoDiaDiem == null)
            {
                return NotFound();
            }
            ViewData["IdCanBo"] = new SelectList(_context.TbCanBos, "IdCanBo", "IdCanBo", tbCanBoDiaDiem.IdCanBo);
            ViewData["IdDiaDiem"] = new SelectList(_context.TbDiaDiemThamNuois, "IdDiaDiem", "IdDiaDiem", tbCanBoDiaDiem.IdDiaDiem);
            return View(tbCanBoDiaDiem);
        }

        // POST: CanBoDiaDiem/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,IdCanBo,IdDiaDiem,ThoiGianBd,ThoiGianKt")] TbCanBoDiaDiem tbCanBoDiaDiem)
        {
            if (id != tbCanBoDiaDiem.Id)
            {
                return NotFound();
            }

           
                try
                {
                    _context.Update(tbCanBoDiaDiem);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TbCanBoDiaDiemExists(tbCanBoDiaDiem.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            
            
        }

        // GET: CanBoDiaDiem/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.TbCanBoDiaDiems == null)
            {
                return NotFound();
            }

            var tbCanBoDiaDiem = await _context.TbCanBoDiaDiems
                .Include(t => t.IdCanBoNavigation)
                .Include(t => t.IdDiaDiemNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tbCanBoDiaDiem == null)
            {
                return NotFound();
            }
            ViewData["IdCanBo"] = new SelectList(_context.TbCanBos, "IdCanBo", "TenCanBo", tbCanBoDiaDiem.IdCanBo);
            ViewData["IdDiaDiem"] = new SelectList(_context.TbDiaDiemThamNuois, "IdDiaDiem", "DiaDiem", tbCanBoDiaDiem.IdDiaDiem);
            return View(tbCanBoDiaDiem);
        }

        // POST: CanBoDiaDiem/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.TbCanBoDiaDiems == null)
            {
                return Problem("Entity set 'DbCrimeManageContext.TbCanBoDiaDiems'  is null.");
            }
            var tbCanBoDiaDiem = await _context.TbCanBoDiaDiems.FindAsync(id);
            if (tbCanBoDiaDiem != null)
            {
                _context.TbCanBoDiaDiems.Remove(tbCanBoDiaDiem);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TbCanBoDiaDiemExists(int id)
        {
          return (_context.TbCanBoDiaDiems?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
