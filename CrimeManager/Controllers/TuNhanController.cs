﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CrimeManager.Models;
using CrimeManager.Helper;

namespace CrimeManager.Controllers
{
    public class TuNhanController : Controller
    {
        private readonly DbCrimeManageContext _context;

        public TuNhanController(DbCrimeManageContext context)
        {
            _context = context;
        }

        // GET: TuNhan
        public async Task<IActionResult> Index()
        {
            var session = HttpContext.Session.GetObject<UserSession>("OpenSession");
            if (session.UserDetail != null)
            {
                if (session.UserDetail.IdUser == 4)
                {
                    ViewBag.ThongTin = await _context.TbThongTinTuNhans.ToListAsync();
                    ViewBag.PhongGiam = await _context.TbPhongGiams.ToListAsync();
                    var dbCrimeManageContext = _context.TbTuNhans.Include(t => t.IdThongTinNavigation);
                    return View(await dbCrimeManageContext.ToListAsync());
                }
                else
                {

                    int idCanBo = session.UserDetail.IdCanBo.Value;
                    ViewBag.ThongTin = await _context.TbThongTinTuNhans.ToListAsync();
                    ViewBag.PhongGiam = await _context.TbPhongGiams.ToListAsync();
                    var idTuNhan = _context.TbCanBoTuNhans.Where(item => item.IdCanBo == session.UserDetail.IdCanBo
                        && DateTime.Now >= item.ThoiGianBd && DateTime.Now <= item.ThoiGianKt).Select(x => x.IdTuNhan).ToList();
                    var dbCrimeManageContext = _context.TbTuNhans.Include(t => t.IdThongTinNavigation).Where(x => idTuNhan.Contains(x.IdTuNhan));
                    return View(await dbCrimeManageContext.ToListAsync());

                }
            }
            return View();
        }

        // GET: TuNhan/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.TbTuNhans == null)
            {
                return NotFound();
            }

            var tbTuNhan = await _context.TbTuNhans
                .Include(t => t.IdThongTinNavigation)
                .FirstOrDefaultAsync(m => m.IdTuNhan == id);
            
            if (tbTuNhan == null)
            {
                return NotFound();
            }
            ViewData["IdThongTin"] = new SelectList(_context.TbThongTinTuNhans, "IdThongTin", "HoTen", tbTuNhan.IdThongTin);
            ViewData["IdPhongGiam"] = new SelectList(_context.TbPhongGiams, "IdPhongGiam", "TenPhong", tbTuNhan.IdPhongGiam);
            return View(tbTuNhan);
        }

        // GET: TuNhan/Create
        public IActionResult Create()
        {
            ViewData["IdThongTin"] = new SelectList(_context.TbThongTinTuNhans, "IdThongTin", "HoTen");
            ViewData["IdPhongGiam"] = new SelectList(_context.TbPhongGiams, "IdPhongGiam", "TenPhong");
            return View();
        }

        // POST: TuNhan/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdTuNhan,IdThongTin,MaTuNhan,IdPhongGiam,NgayRaTu")] TbTuNhan tbTuNhan)
        {
                _context.Add(tbTuNhan);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
        }

        // GET: TuNhan/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.TbTuNhans == null)
            {
                return NotFound();
            }

            var tbTuNhan = await _context.TbTuNhans.FindAsync(id);
            if (tbTuNhan == null)
            {
                return NotFound();
            }
            ViewData["IdPhongGiam"] = new SelectList(_context.TbPhongGiams, "IdPhongGiam", "TenPhong", tbTuNhan.IdPhongGiam);
            ViewData["IdThongTin"] = new SelectList(_context.TbThongTinTuNhans, "IdThongTin", "HoTen", tbTuNhan.IdThongTin);
            return View(tbTuNhan);
        }

        // POST: TuNhan/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdTuNhan,IdThongTin,MaTuNhan,IdPhongGiam,NgayRaTu")] TbTuNhan tbTuNhan)
        {
            if (id != tbTuNhan.IdTuNhan)
            {
                return NotFound();
            }

          
                try
                {
                    _context.Update(tbTuNhan);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TbTuNhanExists(tbTuNhan.IdTuNhan))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            
            
        }

        // GET: TuNhan/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.TbTuNhans == null)
            {
                return NotFound();
            }

            var tbTuNhan = await _context.TbTuNhans
                .Include(t => t.IdThongTinNavigation)
                .FirstOrDefaultAsync(m => m.IdTuNhan == id);
            if (tbTuNhan == null)
            {
                return NotFound();
            }
            ViewData["IdPhongGiam"] = new SelectList(_context.TbPhongGiams, "IdPhongGiam", "TenPhong", tbTuNhan.IdPhongGiam);
            ViewData["IdThongTin"] = new SelectList(_context.TbThongTinTuNhans, "IdThongTin", "HoTen", tbTuNhan.IdThongTin);
            return View(tbTuNhan);
        }

        // POST: TuNhan/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.TbTuNhans == null)
            {
                return Problem("Entity set 'DbCrimeManageContext.TbTuNhans'  is null.");
            }
            var tbTuNhan = await _context.TbTuNhans.FindAsync(id);
            if (tbTuNhan != null)
            {
                _context.TbTuNhans.Remove(tbTuNhan);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TbTuNhanExists(int id)
        {
          return (_context.TbTuNhans?.Any(e => e.IdTuNhan == id)).GetValueOrDefault();
        }
    }
}
