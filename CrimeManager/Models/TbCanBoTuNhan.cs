﻿using System;
using System.Collections.Generic;

namespace CrimeManager.Models;

public partial class TbCanBoTuNhan
{
    public int Id { get; set; }

    public int IdCanBo { get; set; }

    public int IdTuNhan { get; set; }

    public DateTime? ThoiGianBd { get; set; }

    public DateTime? ThoiGianKt { get; set; }

    public virtual TbCanBo IdCanBoNavigation { get; set; } = null!;

    public virtual TbTuNhan IdTuNhanNavigation { get; set; } = null!;
}
