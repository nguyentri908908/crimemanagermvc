﻿using System;
using System.Collections.Generic;

namespace CrimeManager.Models;

public partial class TbCanBo
{
    public int IdCanBo { get; set; }

    public string? TenCanBo { get; set; }

    public string? SoHieuCanBo { get; set; }

    public string? Sdt { get; set; }

    public DateTime? SinhNhat { get; set; }

    public int? CapBac { get; set; }

    public virtual TbCapBac? CapBacNavigation { get; set; }

    public virtual ICollection<TbCanBoDiaDiem> TbCanBoDiaDiems { get; set; } = new List<TbCanBoDiaDiem>();

    public virtual ICollection<TbCanBoNhom> TbCanBoNhoms { get; set; } = new List<TbCanBoNhom>();

    public virtual ICollection<TbCanBoPhongGiam> TbCanBoPhongGiams { get; set; } = new List<TbCanBoPhongGiam>();

    public virtual ICollection<TbCanBoTuNhan> TbCanBoTuNhans { get; set; } = new List<TbCanBoTuNhan>();

    public virtual ICollection<TbUser> TbUsers { get; set; } = new List<TbUser>();
}
