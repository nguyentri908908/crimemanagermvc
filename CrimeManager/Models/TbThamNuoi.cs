﻿using System;
using System.Collections.Generic;

namespace CrimeManager.Models;

public partial class TbThamNuoi
{
    public int IdThamNuoi { get; set; }

    public int? IdToiPham { get; set; }

    public string? TenNguoiThamNuoi { get; set; }

    public string? CccdnguoiTham { get; set; }

    public int? IdDiaDiem { get; set; }

    public DateTime? ThoiGianBd { get; set; }

    public DateTime? ThoiGianKt { get; set; }

    public virtual TbDiaDiemThamNuoi? IdDiaDiemNavigation { get; set; }

    public virtual TbTuNhan? IdToiPhamNavigation { get; set; }
}
