﻿using System;
using System.Collections.Generic;

namespace CrimeManager.Models;

public partial class TbCanBoPhongGiam
{
    public int Id { get; set; }

    public int? IdCanBo { get; set; }

    public int? IdPhongGiam { get; set; }

    public DateTime? ThoiGianBd { get; set; }

    public DateTime? ThoiGianKt { get; set; }

    public virtual TbCanBo? IdCanBoNavigation { get; set; }

    public virtual TbPhongGiam? IdPhongGiamNavigation { get; set; }
}
