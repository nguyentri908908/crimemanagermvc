﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace CrimeManager.Models;

public partial class DbCrimeManageContext : DbContext
{
    public DbCrimeManageContext()
    {
    }

    public DbCrimeManageContext(DbContextOptions<DbCrimeManageContext> options)
        : base(options)
    {
    }

    public virtual DbSet<TbBanAn> TbBanAns { get; set; }

    public virtual DbSet<TbCanBo> TbCanBos { get; set; }

    public virtual DbSet<TbCanBoDiaDiem> TbCanBoDiaDiems { get; set; }

    public virtual DbSet<TbCanBoNhom> TbCanBoNhoms { get; set; }

    public virtual DbSet<TbCanBoPhongGiam> TbCanBoPhongGiams { get; set; }

    public virtual DbSet<TbCanBoTuNhan> TbCanBoTuNhans { get; set; }

    public virtual DbSet<TbCapBac> TbCapBacs { get; set; }

    public virtual DbSet<TbChucNang> TbChucNangs { get; set; }

    public virtual DbSet<TbDiaDiemThamNuoi> TbDiaDiemThamNuois { get; set; }

    public virtual DbSet<TbLoaiBanAn> TbLoaiBanAns { get; set; }

    public virtual DbSet<TbNhomChucNang> TbNhomChucNangs { get; set; }

    public virtual DbSet<TbNhomNguoiDung> TbNhomNguoiDungs { get; set; }

    public virtual DbSet<TbPhongGiam> TbPhongGiams { get; set; }

    public virtual DbSet<TbThamNuoi> TbThamNuois { get; set; }

    public virtual DbSet<TbThongTinTuNhan> TbThongTinTuNhans { get; set; }

    public virtual DbSet<TbTuNhan> TbTuNhans { get; set; }

    public virtual DbSet<TbUser> TbUsers { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseSqlServer("Server=MSI;Database=dbCrimeManage;Persist Security Info=False;user id=admin;password=@Abc123; Encrypt=False");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<TbBanAn>(entity =>
        {
            entity.HasKey(e => e.IdBanAn);

            entity.ToTable("tbBanAn", "TuNhan");

            entity.Property(e => e.NgayBd)
                .HasColumnType("datetime")
                .HasColumnName("NgayBD");

            entity.HasOne(d => d.IdPhamNhanNavigation).WithMany(p => p.TbBanAns)
                .HasForeignKey(d => d.IdPhamNhan)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_tbBanAn_tbThongTinTuNhan");

            entity.HasOne(d => d.LoaiNavigation).WithMany(p => p.TbBanAns)
                .HasForeignKey(d => d.Loai)
                .HasConstraintName("FK_tbBanAn_tbLoaiBanAn");
        });

        modelBuilder.Entity<TbCanBo>(entity =>
        {
            entity.HasKey(e => e.IdCanBo);

            entity.ToTable("tbCanBo");

            entity.Property(e => e.Sdt)
                .HasMaxLength(200)
                .HasColumnName("SDT");
            entity.Property(e => e.SinhNhat).HasColumnType("datetime");
            entity.Property(e => e.SoHieuCanBo).HasMaxLength(200);
            entity.Property(e => e.TenCanBo).HasMaxLength(200);

            entity.HasOne(d => d.CapBacNavigation).WithMany(p => p.TbCanBos)
                .HasForeignKey(d => d.CapBac)
                .HasConstraintName("FK_tbCanBo_tbCapBac");
        });

        modelBuilder.Entity<TbCanBoDiaDiem>(entity =>
        {
            entity.ToTable("tbCanBo_DiaDiem");

            entity.Property(e => e.ThoiGianBd)
                .HasColumnType("datetime")
                .HasColumnName("ThoiGianBD");
            entity.Property(e => e.ThoiGianKt)
                .HasColumnType("datetime")
                .HasColumnName("ThoiGianKT");

            entity.HasOne(d => d.IdCanBoNavigation).WithMany(p => p.TbCanBoDiaDiems)
                .HasForeignKey(d => d.IdCanBo)
                .HasConstraintName("FK_tbCanBo_DiaDiem_tbCanBo");

            entity.HasOne(d => d.IdDiaDiemNavigation).WithMany(p => p.TbCanBoDiaDiems)
                .HasForeignKey(d => d.IdDiaDiem)
                .HasConstraintName("FK_tbCanBo_DiaDiem_tbDiaDiemThamNuoi");
        });

        modelBuilder.Entity<TbCanBoNhom>(entity =>
        {
            entity.ToTable("tbCanBo_Nhom");

            entity.HasOne(d => d.IdCanBoNavigation).WithMany(p => p.TbCanBoNhoms)
                .HasForeignKey(d => d.IdCanBo)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_tbCanBo_Nhom_tbCanBo");

            entity.HasOne(d => d.IdNhomNavigation).WithMany(p => p.TbCanBoNhoms)
                .HasForeignKey(d => d.IdNhom)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_tbCanBo_Nhom_tbNhomNguoiDung");
        });

        modelBuilder.Entity<TbCanBoPhongGiam>(entity =>
        {
            entity.ToTable("tbCanBo_PhongGiam");

            entity.Property(e => e.ThoiGianBd)
                .HasColumnType("datetime")
                .HasColumnName("ThoiGianBD");
            entity.Property(e => e.ThoiGianKt)
                .HasColumnType("datetime")
                .HasColumnName("ThoiGianKT");

            entity.HasOne(d => d.IdCanBoNavigation).WithMany(p => p.TbCanBoPhongGiams)
                .HasForeignKey(d => d.IdCanBo)
                .HasConstraintName("FK_tbCanBo_PhongGiam_tbCanBo");

            entity.HasOne(d => d.IdPhongGiamNavigation).WithMany(p => p.TbCanBoPhongGiams)
                .HasForeignKey(d => d.IdPhongGiam)
                .HasConstraintName("FK_tbCanBo_PhongGiam_tbPhongGiam");
        });

        modelBuilder.Entity<TbCanBoTuNhan>(entity =>
        {
            entity.ToTable("tbCanBo_TuNhan");

            entity.Property(e => e.ThoiGianBd)
                .HasColumnType("datetime")
                .HasColumnName("ThoiGianBD");
            entity.Property(e => e.ThoiGianKt)
                .HasColumnType("datetime")
                .HasColumnName("ThoiGianKT");

            entity.HasOne(d => d.IdCanBoNavigation).WithMany(p => p.TbCanBoTuNhans)
                .HasForeignKey(d => d.IdCanBo)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_tbCanBo_TuNhan_tbCanBo");

            entity.HasOne(d => d.IdTuNhanNavigation).WithMany(p => p.TbCanBoTuNhans)
                .HasForeignKey(d => d.IdTuNhan)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_tbCanBo_TuNhan_tbTuNhan");
        });

        modelBuilder.Entity<TbCapBac>(entity =>
        {
            entity.HasKey(e => e.IdCapBac);

            entity.ToTable("tbCapBac");

            entity.Property(e => e.TenCapBac).HasMaxLength(100);
        });

        modelBuilder.Entity<TbChucNang>(entity =>
        {
            entity.HasKey(e => e.IdChucNang);

            entity.ToTable("tbChucNang");

            entity.Property(e => e.Icon).HasMaxLength(50);
            entity.Property(e => e.TenChucNang).HasMaxLength(200);
        });

        modelBuilder.Entity<TbDiaDiemThamNuoi>(entity =>
        {
            entity.HasKey(e => e.IdDiaDiem);

            entity.ToTable("tbDiaDiemThamNuoi", "DiaDiem");

            entity.Property(e => e.DiaDiem).HasMaxLength(200);
        });

        modelBuilder.Entity<TbLoaiBanAn>(entity =>
        {
            entity.HasKey(e => e.IdLoai).HasName("PK_tbCapDo");

            entity.ToTable("tbLoaiBanAn");

            entity.Property(e => e.Ten).HasMaxLength(50);
        });

        modelBuilder.Entity<TbNhomChucNang>(entity =>
        {
            entity.ToTable("tbNhom_ChucNang");

            entity.HasOne(d => d.IdChucNangNavigation).WithMany(p => p.TbNhomChucNangs)
                .HasForeignKey(d => d.IdChucNang)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_tbNhom_ChucNang_tbChucNang");

            entity.HasOne(d => d.IdNhomNavigation).WithMany(p => p.TbNhomChucNangs)
                .HasForeignKey(d => d.IdNhom)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_tbNhom_ChucNang_tbNhomNguoiDung");
        });

        modelBuilder.Entity<TbNhomNguoiDung>(entity =>
        {
            entity.HasKey(e => e.IdNhom);

            entity.ToTable("tbNhomNguoiDung");

            entity.Property(e => e.TenNhom).HasMaxLength(100);
        });

        modelBuilder.Entity<TbPhongGiam>(entity =>
        {
            entity.HasKey(e => e.IdPhongGiam);

            entity.ToTable("tbPhongGiam", "DiaDiem");

            entity.Property(e => e.TenPhong).HasMaxLength(100);
        });

        modelBuilder.Entity<TbThamNuoi>(entity =>
        {
            entity.HasKey(e => e.IdThamNuoi);

            entity.ToTable("tbThamNuoi", "ThamNuoi");

            entity.Property(e => e.CccdnguoiTham)
                .HasMaxLength(200)
                .HasColumnName("CCCDNguoiTham");
            entity.Property(e => e.TenNguoiThamNuoi).HasMaxLength(200);
            entity.Property(e => e.ThoiGianBd)
                .HasColumnType("datetime")
                .HasColumnName("ThoiGianBD");
            entity.Property(e => e.ThoiGianKt)
                .HasColumnType("datetime")
                .HasColumnName("ThoiGianKT");

            entity.HasOne(d => d.IdDiaDiemNavigation).WithMany(p => p.TbThamNuois)
                .HasForeignKey(d => d.IdDiaDiem)
                .HasConstraintName("FK_tbThamNuoi_tbDiaDiemThamNuoi");

            entity.HasOne(d => d.IdToiPhamNavigation).WithMany(p => p.TbThamNuois)
                .HasForeignKey(d => d.IdToiPham)
                .HasConstraintName("FK_tbThamNuoi_tbTuNhan");
        });

        modelBuilder.Entity<TbThongTinTuNhan>(entity =>
        {
            entity.HasKey(e => e.IdThongTin);

            entity.ToTable("tbThongTinTuNhan", "TuNhan");

            entity.Property(e => e.Cccd)
                .HasMaxLength(200)
                .HasColumnName("CCCD");
            entity.Property(e => e.HoTen).HasMaxLength(200);
            entity.Property(e => e.NgaySinh).HasColumnType("datetime");
            entity.Property(e => e.QueQuan).HasMaxLength(200);
        });

        modelBuilder.Entity<TbTuNhan>(entity =>
        {
            entity.HasKey(e => e.IdTuNhan);

            entity.ToTable("tbTuNhan", "TuNhan");

            entity.Property(e => e.MaTuNhan).HasMaxLength(200);
            entity.Property(e => e.NgayRaTu).HasColumnType("datetime");

            entity.HasOne(d => d.IdPhongGiamNavigation).WithMany(p => p.TbTuNhans)
                .HasForeignKey(d => d.IdPhongGiam)
                .HasConstraintName("FK_tbTuNhan_tbPhongGiam");

            entity.HasOne(d => d.IdThongTinNavigation).WithMany(p => p.TbTuNhans)
                .HasForeignKey(d => d.IdThongTin)
                .HasConstraintName("FK_tbTuNhan_tbThongTinTuNhan");
        });

        modelBuilder.Entity<TbUser>(entity =>
        {
            entity.HasKey(e => e.IdUser);

            entity.ToTable("tbUser");

            entity.Property(e => e.Password).HasMaxLength(200);
            entity.Property(e => e.UserName).HasMaxLength(50);

            entity.HasOne(d => d.IdCanBoNavigation).WithMany(p => p.TbUsers)
                .HasForeignKey(d => d.IdCanBo)
                .HasConstraintName("FK_tbUser_tbCanBo");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
