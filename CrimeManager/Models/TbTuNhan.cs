﻿using System;
using System.Collections.Generic;

namespace CrimeManager.Models;

public partial class TbTuNhan
{
    public int IdTuNhan { get; set; }

    public int? IdThongTin { get; set; }

    public string? MaTuNhan { get; set; }

    public int? IdPhongGiam { get; set; }

    public DateTime? NgayRaTu { get; set; }

    public virtual TbPhongGiam? IdPhongGiamNavigation { get; set; }

    public virtual TbThongTinTuNhan? IdThongTinNavigation { get; set; }

    public virtual ICollection<TbCanBoTuNhan> TbCanBoTuNhans { get; set; } = new List<TbCanBoTuNhan>();

    public virtual ICollection<TbThamNuoi> TbThamNuois { get; set; } = new List<TbThamNuoi>();
}
