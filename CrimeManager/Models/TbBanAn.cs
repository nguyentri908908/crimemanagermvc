﻿using System;
using System.Collections.Generic;

namespace CrimeManager.Models;

public partial class TbBanAn
{
    public int IdBanAn { get; set; }

    public string? NoiDung { get; set; }

    public int IdPhamNhan { get; set; }

    public int? SoNam { get; set; }

    public DateTime? NgayBd { get; set; }

    public int? Loai { get; set; }

    public bool? TrangThai { get; set; }

    public virtual TbThongTinTuNhan IdPhamNhanNavigation { get; set; } = null!;

    public virtual TbLoaiBanAn? LoaiNavigation { get; set; }
}
