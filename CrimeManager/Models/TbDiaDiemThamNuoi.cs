﻿using System;
using System.Collections.Generic;

namespace CrimeManager.Models;

public partial class TbDiaDiemThamNuoi
{
    public int IdDiaDiem { get; set; }

    public string? DiaDiem { get; set; }

    public int? SoCho { get; set; }

    public virtual ICollection<TbCanBoDiaDiem> TbCanBoDiaDiems { get; set; } = new List<TbCanBoDiaDiem>();

    public virtual ICollection<TbThamNuoi> TbThamNuois { get; set; } = new List<TbThamNuoi>();
}
