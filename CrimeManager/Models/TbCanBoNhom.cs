﻿using System;
using System.Collections.Generic;

namespace CrimeManager.Models;

public partial class TbCanBoNhom
{
    public int Id { get; set; }

    public int IdCanBo { get; set; }

    public int IdNhom { get; set; }

    public virtual TbCanBo IdCanBoNavigation { get; set; } = null!;

    public virtual TbNhomNguoiDung IdNhomNavigation { get; set; } = null!;
}
