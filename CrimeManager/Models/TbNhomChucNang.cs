﻿using System;
using System.Collections.Generic;

namespace CrimeManager.Models;

public partial class TbNhomChucNang
{
    public int Id { get; set; }

    public int IdNhom { get; set; }

    public int IdChucNang { get; set; }

    public virtual TbChucNang IdChucNangNavigation { get; set; } = null!;

    public virtual TbNhomNguoiDung IdNhomNavigation { get; set; } = null!;
}
