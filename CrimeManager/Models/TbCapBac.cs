﻿using System;
using System.Collections.Generic;

namespace CrimeManager.Models;

public partial class TbCapBac
{
    public int IdCapBac { get; set; }

    public string? TenCapBac { get; set; }

    public virtual ICollection<TbCanBo> TbCanBos { get; set; } = new List<TbCanBo>();
}
