﻿using System;
using System.Collections.Generic;

namespace CrimeManager.Models;

public partial class TbUser
{
    public int IdUser { get; set; }

    public int? IdCanBo { get; set; }

    public string? UserName { get; set; }

    public string? Password { get; set; }

    public virtual TbCanBo? IdCanBoNavigation { get; set; }
}
