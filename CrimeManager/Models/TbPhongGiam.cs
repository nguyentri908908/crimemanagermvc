﻿using System;
using System.Collections.Generic;

namespace CrimeManager.Models;

public partial class TbPhongGiam
{
    public int IdPhongGiam { get; set; }

    public string? TenPhong { get; set; }

    public int? SoCho { get; set; }

    public bool? TrangThai { get; set; }

    public virtual ICollection<TbCanBoPhongGiam> TbCanBoPhongGiams { get; set; } = new List<TbCanBoPhongGiam>();

    public virtual ICollection<TbTuNhan> TbTuNhans { get; set; } = new List<TbTuNhan>();
}
