﻿using System;
using System.Collections.Generic;

namespace CrimeManager.Models;

public partial class TbNhomNguoiDung
{
    public int IdNhom { get; set; }

    public string? TenNhom { get; set; }

    public virtual ICollection<TbCanBoNhom> TbCanBoNhoms { get; set; } = new List<TbCanBoNhom>();

    public virtual ICollection<TbNhomChucNang> TbNhomChucNangs { get; set; } = new List<TbNhomChucNang>();
}
