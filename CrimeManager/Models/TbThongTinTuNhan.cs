﻿using System;
using System.Collections.Generic;

namespace CrimeManager.Models;

public partial class TbThongTinTuNhan
{
    public int IdThongTin { get; set; }

    public string? HoTen { get; set; }

    public DateTime? NgaySinh { get; set; }

    public string? QueQuan { get; set; }

    public string? Cccd { get; set; }

    public virtual ICollection<TbBanAn> TbBanAns { get; set; } = new List<TbBanAn>();

    public virtual ICollection<TbTuNhan> TbTuNhans { get; set; } = new List<TbTuNhan>();
}
