﻿using System;
using System.Collections.Generic;

namespace CrimeManager.Models;

public partial class TbChucNang
{
    public int IdChucNang { get; set; }

    public int? IdChucNangCha { get; set; }

    public string? TenChucNang { get; set; }

    public string? DuongDan { get; set; }

    public string? Icon { get; set; }

    public virtual ICollection<TbNhomChucNang> TbNhomChucNangs { get; set; } = new List<TbNhomChucNang>();
}
