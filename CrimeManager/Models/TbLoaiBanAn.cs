﻿using System;
using System.Collections.Generic;

namespace CrimeManager.Models;

public partial class TbLoaiBanAn
{
    public int IdLoai { get; set; }

    public string? Ten { get; set; }

    public virtual ICollection<TbBanAn> TbBanAns { get; set; } = new List<TbBanAn>();
}
