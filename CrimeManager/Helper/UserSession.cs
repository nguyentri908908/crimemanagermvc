﻿using CrimeManager.Models;
using System.Text.Json;

namespace CrimeManager.Helper
{
    public class UserSession
    {
        public DateTime StartedAt { get; set; }
        public TbUser UserDetail { get; set; }

        public UserSession()
        {
        }

        public UserSession(TbUser user)
        {
            StartedAt = DateTime.UtcNow;
            UserDetail = user;
        }
    }

    public static class SessionExtensions
    {
        public static void SetObject<T>(this ISession session, string key, T value)
        {
            session.SetString(key, JsonSerializer.Serialize(value));
        }
        public static T GetObject<T>(this ISession session, string key)
        {
            var value = session.GetString(key);
            return value == null ? default : JsonSerializer.Deserialize<T>(value);
        }
    }
}
